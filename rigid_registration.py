import math
import shutil
import tempfile

import SimpleITK as sitk

import utils
from anima_utils import *


def sort_images(image1, image2, axis):
	return (image1, image2) if image1.GetSize()[axis] * image1.GetSpacing()[axis] > image2.GetSize()[axis] * image2.GetSpacing()[axis] else (image2, image1)

def get_image_patches(image, patch_size, axis, n=None):
	images = []
	image_size = image.GetSize()
	n = n or ( image_size[axis] // patch_size ) + 1
	length = image_size[axis] - patch_size
	source_index = [0, 0, 0]
	source_index[axis] = patch_size // 2
	size = list(image_size)
	size[axis] = min(patch_size, image_size[axis])
	# pif = sitk.PasteImageFilter()
	for i in range(n):
		source_index[axis] = i * length // (n-1) if n > 1 else 0
		images.append(utils.get_patch(image, source_index, size))

	return images

def register_anima(fixed_path, moving_path, output_path, output_transform_path, args=None):
	args = args or utils.configuration['default_rigid_registration_args']
	pyramid_options = get_registration_options_from_image(fixed_path) if '-p' not in args and '-l' not in args else []
	call([animaPyramidalBMRegistration, '-r', fixed_path, '-m', moving_path, '-o', output_path, '-O', output_transform_path] + args + pyramid_options)
	return output_path, output_transform_path

def register_elastix(fixed_path, moving_path, output_path, output_transform_path, args=None):
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_folder = Path(tmpdirname)
		subprocess.call(['elastix', '-f', fixed_path, '-m', moving_path, '-out', output_folder, '-p', 'rigid_transform_parameters.txt'] + args)
		registered = sitk.ReadImage(str(output_folder / 'result.0.nii'))
		sitk.WriteImage(registered, str(output_path))
		shutil.copyfile(output_folder / 'TransformParameters.0.txt', output_transform_path)
	return output_path, output_transform_path

def register_anima_and_read_outputs(fixed_path, moving_path, args=None):
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_path = Path(tmpdirname) / 'output.nii.gz'
		output_transform_path = Path(tmpdirname) / 'output.txt'
		register_anima(fixed_path, moving_path, output_path, output_transform_path, args)
		output = sitk.ReadImage(str(output_path), sitk.sitkFloat32)
		output_transform = sitk.ReadTransform(str(output_transform_path))
		return output, output_transform

def register_anima_from_image_and_read_outputs(fixed, moving, args=None):
	with tempfile.TemporaryDirectory() as tmpdirname:
		fixed_path = Path(tmpdirname) / 'fixed.nii.gz'
		moving_path = Path(tmpdirname) / 'moving.nii.gz'
		sitk.WriteImage(fixed, str(fixed_path))
		sitk.WriteImage(moving, str(moving_path))
		return register_anima_and_read_outputs(fixed_path, moving_path, args)

def crop_image_from_reference(reference, image, axis):
	size1 = reference.GetSize()
	size2 = image.GetSize()
	small_size = [min(size1[i], size2[i]) for i in range(3)]
	small_size[axis] = size2[axis]
	source_index = [math.floor(size2[i] / 2 - small_size[i] / 2) for i in range(3)]
	return utils.get_patch(image, source_index, small_size)

def get_images_same_size(image1, image2, axis):
	i1 = crop_image_from_reference(image2, image1, axis)
	i2 = crop_image_from_reference(image1, image2, axis)
	return i1, i2

def get_images_same_spacing(image1, image2, axis = 1):
	if abs(image1.GetSpacing()[axis] - image2.GetSpacing()[axis]) > 1e-3:
		min_spacing = np.minimum(image1.GetSpacing(), image2.GetSpacing())
		image1 = utils.set_spacing(image1, min_spacing)
		image2 = utils.set_spacing(image2, min_spacing)
	return image1, image2

def order_list_from_middle_to_ends(l):
	return [l[len(l) // 2 + (1 if i % 2 == 0 else -1) * i // 2] for i in range(len(l))]

# Patches will be extracted from fixed_path and moving_path, a registration will be computed for each pair of patches
def register_patches(fixed, moving, split_big_only=True, axis=None, n_patches=None, similarity_early_stop_threshold=None):
	axis = axis or utils.configuration['axis']
	n_patches = n_patches or utils.configuration['n_patches']
	similarity_early_stop_threshold = similarity_early_stop_threshold or utils.configuration['similarity_early_stop_threshold']
	fixed, moving = get_images_same_spacing(fixed, moving, axis)

	big_image, small_image = sort_images(fixed, moving, axis)

	fixed_is_big = big_image is fixed

	big_image, small_image = get_images_same_size(big_image, small_image, axis)

	big_patches = []
	small_patches = []

	if split_big_only:
		big_patches = get_image_patches(big_image, small_image.GetSize()[axis], axis)
		small_patches = [small_image]
	else:
		big_patches = get_image_patches(big_image, min(big_image.GetSize()[axis]//2, small_image.GetSize()[axis]), axis, n=n_patches)
		small_patches = get_image_patches(small_image, big_patches[0].GetSize()[axis], axis)

	big_patches = order_list_from_middle_to_ends(big_patches)
	small_patches = order_list_from_middle_to_ends(small_patches)

	registrations = []

	for i, big_patch in enumerate(big_patches):
		for j, small_patch in enumerate(small_patches):
			print(f'    register patches {i} and {j}...')
			fixed_patch, moving_patch = (big_patch, small_patch) if fixed_is_big else (small_patch, big_patch)
			moving_patch_registered, transform = register_anima_from_image_and_read_outputs(fixed_patch, moving_patch)
			moving_registered = utils.apply_transform(fixed, moving, transform)
			
			similarity = utils.compute_similarity(fixed, moving_registered, match_image_path=utils.configuration['output_folder'] / f'image_{i}_on_{j}.png' if utils.configuration['debug'] else None)
			
			# Recompute similarity on patches only (not full images) to better visualize what has been registered
			if utils.configuration['debug']:
				utils.compute_similarity(fixed_patch, moving_patch_registered, match_image_path=utils.configuration['output_folder'] / f'patch_{i}_on_{j}.png' if utils.configuration['debug'] else None)
			
			registrations.append({ 
				'transform': transform, 
				'name': 'patches',
				'similarity': similarity,
				'fixed': fixed,
				'moving': moving,
				'moving_registered': moving_registered,
			})
			if similarity <= similarity_early_stop_threshold:
				return registrations

	return registrations

def register_intersection(fixed_resampled, moving_resampled, moving_registered, transform, axis=None, ratio_to_remove=None):
	axis = axis or utils.configuration['axis']
	ratio_to_remove = ratio_to_remove or utils.configuration['final_registration_intersection_ratio']
	bounding_box1 = utils.get_bounding_box_from_content(fixed_resampled)
	bounding_box2 = utils.get_bounding_box_from_content(moving_registered)
	intersection = utils.get_intersection([bounding_box1, bounding_box2])
	axis_size = intersection[2*axis+1] - intersection[2*axis]
	amount_to_remove = int(axis_size * ratio_to_remove)
	intersection[2*axis] += amount_to_remove
	intersection[2*axis+1] -= amount_to_remove
	fixed_patch = utils.get_patch_from_bounding_box(fixed_resampled, intersection)
	moving_patch = utils.get_patch_from_bounding_box(moving_registered, intersection)
	moving_patch_registered, new_transform = register_anima_from_image_and_read_outputs(fixed_patch, moving_patch, ['-I', 0])
	# new_moving_registered = utils.apply_transform(fixed, moving_registered, new_transform)
	composite_transform = sitk.CompositeTransform([transform, new_transform])
	new_moving_registered = utils.apply_transform(fixed_resampled, moving_resampled, composite_transform)
	return new_moving_registered, composite_transform

def register_intersection_and_keep_best(fixed_resampled, moving_resampled, moving_registered, transform, similarity):
	new_moving_registered, new_transform = register_intersection(fixed_resampled, moving_resampled, moving_registered, transform)
	new_similarity = utils.compute_similarity(fixed_resampled, new_moving_registered, match_image_path=utils.configuration['output_folder'] / f'final_registration.png' if utils.configuration['debug'] else None)
	return (new_moving_registered, new_transform, new_similarity) if new_similarity < similarity else (moving_registered, transform, similarity)

def register_rigid(fixed, moving):

	registrations = register_patches(fixed, moving, split_big_only=False)
	
	if len(registrations) == 0:
		return None

	registrations.sort(key=lambda x: x['similarity'])

	best_registration = registrations[0]

	moving_registered = best_registration['moving_registered']
	fixed_resampled = best_registration['fixed']
	moving_resampled = best_registration['moving']
	similarity = best_registration['similarity']
	transform = best_registration['transform']

	moving_registered, transform, similarity = register_intersection_and_keep_best(fixed_resampled, moving_resampled, moving_registered, transform, similarity)

	return fixed_resampled, moving_resampled, moving_registered, transform, similarity

def register_rigid_from_paths(fixed_path, moving_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path):
	
	fixed = sitk.ReadImage(str(fixed_path), sitk.sitkFloat32)
	moving = sitk.ReadImage(str(moving_path), sitk.sitkFloat32)
	
	fixed_resampled, moving_resampled, moving_registered, transform, similarity = register_rigid(fixed, moving)

	sitk.WriteImage(fixed_resampled, str(fixed_resampled_path))
	sitk.WriteImage(moving_resampled, str(moving_resampled_path))
	sitk.WriteImage(moving_registered, str(moving_registered_path))
	sitk.WriteTransform(transform, str(transform_path))

	utils.edit_json(similarity_path, 'rigid_similarity', similarity)

	return fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path