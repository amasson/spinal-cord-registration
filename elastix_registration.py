import shutil
import subprocess
import tempfile
from pathlib import Path

import SimpleITK as sitk

import rigid_registration

# import itk
# parameter_object = itk.ParameterObject.New()
# default_rigid_parameter_map = parameter_object.GetDefaultParameterMap('rigid')
# parameter_object.WriteParameterFile(default_rigid_parameter_map, 'rigid_transform_parameters.txt')
# default_bspline_parameter_map = parameter_object.GetDefaultParameterMap('bspline')
# parameter_object.WriteParameterFile(default_bspline_parameter_map, 'bspline_transform_parameters.txt')

# # default_bspline_parameter_map['Metric'] = ("TransformBendingEnergyPenalty")
# default_bspline_parameter_map['Metric'] = ("AdvancedMattesMutualInformation", "TransformBendingEnergyPenalty")
# # default_bspline_parameter_map['Metric'] = ("AdvancedMattesMutualInformation", "TransformBendingEnergyPenalty", "DisplacementMagnitudePenalty")
# # default_bspline_parameter_map["Metric2Weight"] = "1000"
# # default_bspline_parameter_map["Metric3Weight"] = "10"
# default_bspline_parameter_map["Metric2Weight"] = "10"
# parameter_object.AddParameterMap(default_bspline_parameter_map)


def register_rigid_from_paths(fixed_reoriented_path, moving_reoriented_path, fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path):
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_folder = Path(tmpdirname)
		subprocess.call(['elastix', '-f', fixed_reoriented_path, '-m', moving_reoriented_path, '-out', output_folder, '-p', 'rigid_transform_parameters.txt'])
		registered = sitk.ReadImage(str(output_folder / 'result.0.nii'))
		sitk.WriteImage(registered, str(moving_registered_path))
		shutil.copyfile(output_folder / 'TransformParameters.0.txt', transform_path)
	# rigid_registration.register_anima(fixed_reoriented_path, moving_reoriented_path, moving_registered_path, transform_path)
	return fixed_resampled_path, moving_resampled_path, moving_registered_path, transform_path, similarity_path


def register_non_linear_from_paths(fixed_resampled_path, moving_resampled_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, part_folder, similarity_path, keep_intermediate_files):
	# fixed_masked_path, moving_registered_masked_path = utils.mask_images_intersection(fixed_resampled_path, moving_registered_path, threshold=1)
	with tempfile.TemporaryDirectory() as tmpdirname:
		output_folder = Path(tmpdirname)
		fixed_masked_path = output_folder / 'fixed_masked.nii.gz'

		subprocess.call(['animaMaskImage', '-i', fixed_resampled_path ,'-m', moving_registered_path, '-o', fixed_masked_path])

		subprocess.call(['elastix', '-f', fixed_resampled_path, '-m', moving_resampled_path, '-fMask', fixed_masked_path, '-out', output_folder, '-p', 'bspline_transform_parameters.txt', '-t0', transform_path.resolve()])
		registered = sitk.ReadImage(str(output_folder / 'result.0.nii'))
		sitk.WriteImage(registered, str(moving_registered_nl_path))
		shutil.copyfile(output_folder / 'TransformParameters.0.txt', transform_nl_path)
		# subprocess.call(['transformix', '-in', fixed_resampled_path, '-out', output_folder, '-tp', transform_nl_path])
	return
