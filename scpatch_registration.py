import cv2 as cv
import argparse
import math
import utils
from anima_utils import *
import SimpleITK as sitk

from rigid_registration import get_images_same_size, get_images_same_spacing, register_anima_from_image_and_read_outputs
from non_linear_registration import register_non_linear_from_paths, register_anima_non_linear, bspline_intra_modal_registration

# orb = cv.ORB_create()
# orb_bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)

# sift = cv.SIFT_create(contrastThreshold=0.03, edgeThreshold=10)
sift = cv.SIFT_create()
sift_bf = cv.BFMatcher()

def reorient_image(image):
	if sitk.DICOMOrientImageFilter_GetOrientationFromDirectionCosines(image.GetDirection()) != 'LPS':
		image = sitk.DICOMOrient(image, 'LPS')
	return image

def get_patch_center_from_centerline_z(image, centerline, z):
	voxel_in_centerline = list(centerline.TransformPhysicalPointToIndex(image.TransformIndexToPhysicalPoint([0, 0, z])))
	centerline_slice = sitk.GetArrayFromImage(centerline[:,:,voxel_in_centerline[2]])
	x, y = np.unravel_index(np.argmax(centerline_slice, axis=None), centerline_slice.shape)
	voxel_in_centerline[0] = int(x)
	voxel_in_centerline[1] = int(y)
	voxel_in_image = image.TransformPhysicalPointToIndex(centerline.TransformIndexToPhysicalPoint(voxel_in_centerline))
	return voxel_in_image

def get_patch(image, source_index, size):
	rif = sitk.ResampleImageFilter()
	rif.SetReferenceImage(image)
	rif.SetOutputOrigin(image.TransformIndexToPhysicalPoint(source_index))
	rif.SetSize(size)
	return rif.Execute(image)

def get_image_patches(image, centerline, patch_size, axis, min_overlap):
	images = []
	source_index = [0, 0, 0]
	image_size = list(image.GetSize())
	size = [min(patch_size, s) for s in image_size]
	length = image_size[axis]
	# size[axis] = min(patch_size, length)
	n = math.ceil( (length - min_overlap * patch_size)  / (patch_size*(1-min_overlap)) )
	for i in range(n):
		z = (i * (length - patch_size)) // (n - 1) if n>1 else 0
		center_voxel = get_patch_center_from_centerline_z(image, centerline, z)
		source_index = [int(cv-patch_size/2) for cv in center_voxel]
		images.append(get_patch(image, source_index, size))
	return images

def compute_similarity(image1, image2):
	
	image1_data = sitk.GetArrayFromImage(image1)
	image2_data = sitk.GetArrayFromImage(image2)
	image1_data_0 = utils.convert_to_uint8(image1_data[image1_data.shape[0]//2,:,:])
	image2_data_0 = utils.convert_to_uint8(image2_data[image2_data.shape[0]//2,:,:])

	distance = 0

	kp1, des1 = sift.detectAndCompute(image1_data_0,None)
	kp2, des2 = sift.detectAndCompute(image2_data_0,None)
	
	if des1 is None or des2 is None or len(des1) == 0 or len(des2) == 0 or des1.dtype != des2.dtype:
		return 0
	
	matches = sift_bf.knnMatch(des1,des2,k=2)
	# matches = matcher.match(des1,des2)
	best_matches = [m for m,n in matches if m.distance < 0.75 * n.distance]
	return len(best_matches)

# Patches will be extracted from fixed_path and moving_path, a registration will be computed for each pair of patches
def register_patches(fixed, moving, fixed_centerline, moving_centerline, patch_size_mm=100, patch_overlap=0.75, axis=None, similarity_early_stop_threshold=None):
	axis = axis or utils.configuration['axis']
	similarity_early_stop_threshold = similarity_early_stop_threshold or utils.configuration['similarity_early_stop_threshold']
	
	fixed = reorient_image(fixed)
	moving = reorient_image(moving)
	fixed_centerline = reorient_image(fixed_centerline)
	moving_centerline = reorient_image(moving_centerline)
	
	fixed, moving = get_images_same_spacing(fixed, moving, axis)
	
	# Get patches along centerline
	# For each patch: until similarity is too low: find best remaining matching patch and register
	# Interpolate between all this registrations

	axis_spacing = fixed.GetSpacing()[axis]
	patch_size = patch_size_mm // axis_spacing
	
	fixed_patches = get_image_patches(fixed, patch_size, axis, patch_overlap)
	moving_patches = get_image_patches(moving, patch_size, axis, patch_overlap)

	fixed_path = utils.configuration['output_folder'] / f'fixed.nii.gz'
	moving_path = utils.configuration['output_folder'] / f'moving.nii.gz'

	sitk.WriteImage(fixed, str(fixed_path))
	sitk.WriteImage(moving, str(moving_path))
	
	registered = sitk.Image(fixed.GetSize(), sitk.sitkFloat64)
	registered.CopyInformation(fixed)

	registered_nl = sitk.Image(fixed.GetSize(), sitk.sitkFloat64)
	registered_nl.CopyInformation(fixed)
	
	pif = sitk.PasteImageFilter()
	
	similarity_threshold = 10

	for i, fixed_patch in enumerate(fixed_patches):

		# Compute similarities
		similarities = []
		for j, moving_patch in enumerate(moving_patches):
			similarities.append((j, compute_similarity(fixed_patch, moving_patch)))
		
		similarities.sort(key=lambda x: x[1])
		final_similarity = None
		moving_patch_index = -1
		moving_patch_registered = None
		moving_registered = None
		transform = None
		while final_similarity is None or final_similarity < similarity_threshold:
			moving_patch_index = similarities.pop(0)[0]
			moving_patch = moving_patches[moving_patch_index]
			moving_patch_registered, transform = register_anima_from_image_and_read_outputs(fixed_patch, moving_patch)
			moving_registered = utils.apply_transform(fixed, moving, transform)

			final_similarity = utils.compute_similarity(fixed, moving_registered, match_image_path=utils.configuration['output_folder'] / f'image_{i}_on_{j}.png' if utils.configuration['debug'] else None)

		pi = i
		pj = j

		fixed_patch_path = utils.configuration['output_folder'] / f'fixed_patch_{pi}_on_{pj}.nii.gz'
		moving_registered_patch_path = utils.configuration['output_folder'] / f'moving_registered_patch_{pi}_on_{pj}.nii.gz'

		sitk.WriteImage(moving_patch_registered, str(moving_registered_patch_path))
		sitk.WriteImage(fixed_patch, str(fixed_patch_path))

		moving_registered_path = utils.configuration['output_folder'] / f'moving_registered_{pi}_on_{pj}.nii.gz'
		
		sitk.WriteImage(moving_registered, str(moving_registered_path))

		moving_registered_nl_path = utils.configuration['output_folder'] / f'moving_registered_nl_{pi}_on_{pj}.nii.gz'
		transform_nl_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_transform_nl.nii.gz'
		
		transform_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_transform.txt'
		similarity_path = utils.configuration['output_folder'] / f'patch_{pi}_to_{pj}_similarity.json'

		sitk.WriteTransform(transform, str(transform_path))

		register_non_linear_from_paths(fixed_path, moving_path, moving_registered_path, moving_registered_nl_path, transform_path, transform_nl_path, utils.configuration['output_folder'], similarity_path, keep_intermediate_files=False)

		registered_patch = moving_patch_registered 
		moving_registered_nl = sitk.ReadImage(str(moving_registered_nl_path), sitk.sitkFloat64)
		moving_registered = sitk.Cast(moving_registered, sitk.sitkFloat64)

		source_index = [0, 0, 0]
		registered_patch_size = list(registered_patch.GetSize())
		source_index[axis] = int(0.25 * registered_patch_size[axis]) if i>0 else 0
		source_size = list(registered_patch_size)
		source_size[axis] = int((0.5 if i>0 and i<len(fixed_patches)-1 else 0.75) * source_size[axis])
		
		index = registered.TransformPhysicalPointToIndex(registered_patch.TransformIndexToPhysicalPoint(source_index))
		pif.SetSourceIndex(index)
		pif.SetSourceSize(source_size)
		pif.SetDestinationIndex(index)
		
		
		registered = pif.Execute(registered, moving_registered)
		registered_nl = pif.Execute(registered_nl, moving_registered_nl)
		# sitk.WriteImage(registered, str(utils.configuration['output_folder'] / f'registered_{i}.nii.gz'))
		# sitk.WriteImage(registered_nl, str(utils.configuration['output_folder'] / f'registered_nl_{i}.nii.gz'))
	
	sitk.WriteImage(registered, str(utils.configuration['output_folder'] / f'registered.nii.gz'))
	sitk.WriteImage(registered_nl, str(utils.configuration['output_folder'] / f'registered_nl.nii.gz'))

	return


if __name__ == '__main__':

	parser = argparse.ArgumentParser(
		prog=__file__,
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
		description="""Register rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).""")

	parser.add_argument('-f', '--fixed', required=True, help='Path to the reference image.')
	parser.add_argument('-m', '--moving', required=True, help='Path to the image to register.')
	parser.add_argument('-ps', '--patch_size', type=float, default=100, help='Patch size.')
	parser.add_argument('-ov', '--overlap', type=float, default=0.75, help='Patch overlap.')
	parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')

	utils.add_args(parser)

	args = parser.parse_args()

	utils.load_config(args.configuration)

	output_folder = Path(args.output_folder) if args.output_folder else Path()
	output_folder.mkdir(exist_ok=True, parents=True)

	utils.set_debug(output_folder, args)
	

	fixed_path = Path(args.fixed)
	moving_path = Path(args.moving)

	
	fixed = sitk.ReadImage(str(fixed_path))
	moving = sitk.ReadImage(str(moving_path))

	fixed_centerline_path = fixed_path.parent / fixed_path.name.replace('.nii.gz', '_centerline.nii.gz')
	moving_centerline_path = moving_path.parent / moving_path.name.replace('.nii.gz', '_centerline.nii.gz')
	call(['sct_get_centerline', '-i', fixed_path, '-c', 't2', '-o', fixed_centerline_path])
	call(['sct_get_centerline', '-i', moving_path, '-c', 't2', '-o', moving_centerline_path])
	
	fixed_centerline = sitk.ReadImage(str(fixed_centerline_path))
	moving_centerline = sitk.ReadImage(str(moving_centerline_path))

	register_patches(fixed, moving, fixed_centerline, moving_centerline, similarity_early_stop_threshold='disabled', patch_size_mm=args.patch_size, patch_overlap=args.overlap)