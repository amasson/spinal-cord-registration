from pathlib import Path
import argparse
import utils
import SimpleITK as sitk
from rigid_registration import register_rigid_from_paths
from non_linear_registration import register_non_linear_from_paths

parser = argparse.ArgumentParser(
    prog=__file__,
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    description="""Apply a transformation (computed computed with register.py or register_batch.py). Input images will be reoriented.""")

parser.add_argument('-t', '--transforms', required=True, help='List of transformations to apply. Can be paths to rigid and non linear transformations (usually something like "rigid_transform.txt non_linear_transform.nii.gz").', nargs='+')

parser.add_argument('-f', '--fixed', required=True, help='Path to the reference image.')
parser.add_argument('-m', '--moving', required=True, help='Path to the image to register.')
parser.add_argument('-o', '--output_folder', default=None, help='Path to the output folder. Default is the current directory.')

parser.add_argument('-i', '--interpolation', default=2, type=int, help='Interpolation to use. Use 1 (NearestNeighbor) for label images like segmentations and 2 (Linear) of other images. See itk::simple::InterpolatorEnum to see the other possible interpolations.')

utils.add_args(parser)

args = parser.parse_args()

utils.load_config(args.configuration)

fixed_path = Path(args.fixed)
moving_path = Path(args.moving)
output_folder = Path(args.output_folder) if args.output_folder else Path()
output_folder.mkdir(exist_ok=True, parents=True)

utils.set_debug(output_folder, args)

moving_registered_nl_path = utils.set_output_file_path(args.moving_registered_nl, output_folder, moving_path.name.replace('.nii.gz', '_registered_nl.nii.gz'))

fixed_reoriented_path = utils.reorient_image(fixed_path, output_folder / fixed_path.name.replace('.nii.gz', '_fixed_reoriented.nii.gz'))
moving_reoriented_path = utils.reorient_image(moving_path, output_folder / moving_path.name.replace('.nii.gz', '_moving_reoriented.nii.gz'))

utils.apply_transforms_sitk(fixed_reoriented_path, moving_reoriented_path, args.transforms, moving_registered_nl_path, args.interpolation)

