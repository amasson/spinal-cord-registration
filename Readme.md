# Spinal Cord Registration

A tool to register two spinal cord images.

It registers rigidly and non-linearly two spinal cord images from a given patient. The method is designed to be robust to difference of sagital coverage and to partial overlap in the two images. However, images must have similar intensity characteristics (typically same sequence).


## How does it work?

Spinal cord images can be in very different spaces (with different origins and orientations) from one time point to another. Moreover, two time points can only partially overlap. 
A standard rigid registration is likely to fail in such hard cases. 

The approach used to register the time points is to split both images in patches (along the spinal cord), register the obtained pair of patches, and take the best transform (which gives the best image alignment according to a custom similarity metric).
The similarity metric is the number of SIFT and ORB feature points ([computed with OpenCV](https://docs.opencv.org/3.4/db/d27/tutorial_py_table_of_contents_feature2d.html)) in the 2D center sagittal slice, which match in both images (two feature points match when they have the same position in both images).

Finally, since we would prefer the images to be perfectly aligned in the center (instead of perfectly aligned at the top, and poorly at the bottom) we try a last registration with centered patches, initialized with the previous best transform. If this last registration gives a better image alignment, we keep the obtained transformation, otherwise we ignore it.

Here are a few example of feature matching:

- SIFT feature matches on the resulting images (resampled fixed image, resampled and registered moving image), the registration is computed from the patch 0 of fixed image and patch 0 of the moving image:
   ![SIFT features matched on the patches 0 and 0](images/patch_0_on_0_sift_all.png)

- Same as above, but computed from the patch 0 of fixed image and patch 1 of the moving image:
   ![SIFT features matched on the patches 0 and 1](images/patch_0_on_1_sift_all.png)

- Same as above, but computed from the center patch of fixed image and the center patch of the moving image:
   ![SIFT features matched on the patches 1 and 1](images/final_registration_sift_all.png)

Once the rigid registration is computed, a non linear registration is computed with anima or elastix, or both. Anima will compute a dense deformation field, elastix will compute a bspline transformation. 

## Install

 - [Install the Spinal Cord Toolbox](https://spinalcordtoolbox.com/#installation) by downloading [the latest release](https://github.com/spinalcordtoolbox/spinalcordtoolbox/releases) and running `./install_sct`
 - Install [anima](https://anima.readthedocs.io/en/latest/) (for example from [binaries](https://anima.readthedocs.io/en/latest/install_binaries.html) and with [Anima Scripts](https://anima.readthedocs.io/en/latest/install_anima_scripts.html))
 - Download and install [elastix](https://elastix.lumc.nl/index.php), only required to compute bspline transformations (for example from [Github](https://github.com/SuperElastix/elastix/releases/), just extract the release and add the `bin/` folder to your `$PATH`)
 - (optional but recommended) create a virtual environment with `python -m venv spinal_cord_registration` and activate it with `source spinal_cord_registration/bin/activate` (remember to always activate your environment before using it)
 - install the latest SimpleITK version with `python -m pip install --pre SimpleITK --find-links https://github.com/SimpleITK/SimpleITK/releases/tag/v2.2rc2`
 - install the other dependencies (numpy and opencv) `pip install -r requirements.txt`

## Usage

`python register.py -f path/to/reference.nii.gz -m path/to/image.nii.gz -o path/to/output/folder`, `-f` and `-m` being the fixed and moving images respectively.

The following files will be generated in the output folder in the following order:
 - `fixedName_fixed_reoriented.nii.gz`: the fixed image reoriented (this image will be deleted after execution if `keep_intermediate_files` is not set),
 - `movingName_moving_reoriented.nii.gz,` the moving image reoriented (this image will be deleted after execution if `keep_intermediate_files` is not set),
 - `fixedName_resampled.nii.gz`: the fixed image resampled (so that both images are in the same space),
 - `movingName_resampled.nii.gz`: the moving image resampled (so that both images are in the same space),
 - `movingName_registered.nii.gz`: the moving image registered rigidly,
 - `movingName_registered_nl.nii.gz`: the moving image registered rigidly and non linearly,
 - `transform.nii.gz`: the rigid transform to go from the fixed image to the moving image,
 - `transform_nl.nii.gz`: the non-linear transform,
 - `similarity.txt`: the similarity measures.

Output file names can be modified with the corresponding arguments (`--fixed_resampled` enables to set the name of the resampled fixed image, etc.).

Use `python register.py --help` to get more help.

### Batch usage

The images must be structured in the following manner:
```
patients_to_register/
├── patient01
│   ├── 2017-04-19
│   │   ├── 0.nii.gz
│   │   ├── 1.nii.gz
│   │   ├── 2.nii.gz
│   │   └── combined.nii.gz
│   └── 2018-05-03
│   │   ├── 0.nii.gz
│   │   ├── 1.nii.gz
│   │   ├── 3.nii.gz
│   │   └── combined.nii.gz
│   └── 2019-05-03
│       ├── 0.nii.gz
│       ├── 1.nii.gz
│       ├── 2.nii.gz
│       ├── 3.nii.gz
│       └── combined.nii.gz
├── patient02
│...
```

The first time point will be registered on the newer ones.
Images with the same name in different time points will be registered together.

In the example above :
`patient01/2017-04-19/0.nii.gz` will be registered on `patient01/2018-05-03/0.nii.gz`
`patient01/2017-04-19/0.nii.gz` will be registered on `patient01/2019-05-03/0.nii.gz`
`patient01/2017-04-19/1.nii.gz` will be registered on `patient01/2018-05-03/1.nii.gz`
`patient01/2017-04-19/1.nii.gz` will be registered on `patient01/2019-05-03/1.nii.gz`
`patient01/2017-04-19/2.nii.gz` will be registered on `patient01/2019-05-03/2.nii.gz`
`patient01/2018-05-03/3.nii.gz` will be registered on `patient01/2019-05-03/3.nii.gz`

The similarity measures will be recorded in the `similarities.csv` file.

The output file names will follow the same patterns as when processing a single patient.

### Notes

The tool can be configured with the configuration file (`config.yml` by default) and the following arguments:

```
  -kif, --keep_intermediate_files
                        Keep intermediate files. (default: False)
  -sr, --skip_rigid_if_exists
                        Skip rigid registration if output files already exist. (default: False)
  -cp, --create_previews
                        Create image previews. (default: False)
  -cfg CONFIGURATION, --configuration CONFIGURATION
                        Path to the configuration file. (default: config.yml)
  -dbg, --debug         Debug will output previews of the feature matches. (default: False)
```